
<ul class="nav">
	<li class="nav-item">
		<a class="nav-link" href="index.php">Home</a>
	</li>	
	<li class="nav-item">
		<a class="nav-link" href="page1.php">Page 1</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="page2.php">Page 2</a>
	</li>
	
	<?php

		if(empty($_SESSION['username'])){

			//display only if not logged in
			echo 	'<li class="nav-item">
						<a class="nav-link" href="register.php">Register</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="login.php">Login</a>
					</li>';

		} else {

			//display only when logged in	
			echo 	'<li class="nav-item">
						<a class="nav-link" href="index.php?action=logout">Logout</a>
					</li>
					<li class="nav-item disabled">
						<a class="nav-link disabled" href="#">Hello '.$_SESSION['username'].'</a>
					</li>';

		}
	?>
	
</ul> 

