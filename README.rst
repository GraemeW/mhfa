v0.1
----

Notes:

- User credentials are stored in the 'users' table with the default password_hash algorithm

- A test user (username='Testuser', pw='password') is created automatically

- Passwords are not protected in transit by https, but would be in a production environment

- Login and Registration pages are created separately to the Home page in order to keep each page as simple as possible

- Large sections of PHP code have been moved to .inc files for readability and re-usability

- The 'genders' table has been introduced to demonstrate simple relational data structure