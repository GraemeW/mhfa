<?php

	// open database and initialize tables if needed
	include 'initdb.inc';

?>
<!doctype html>

<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<title>Basic PHP Page - Login</title>
		<link rel="stylesheet" href="mhfa.css">
	</head>

	<body>

		<div class="container">
			
			<h1>Registration Form</h1>
			<?php
				if(isset($_GET['invalid'])){
					echo'<br><div class="alert alert-danger">Invalid username, password or gender.</div>';
				}
				if(isset($_GET['missing'])){
					echo'<br><div class="alert alert-danger">Missing username, password or gender. All fields a mandatory.</div>';
				}
				if(isset($_GET['used'])){
					echo'<br><div class="alert alert-danger">Selected username is already in use. Choose a different username.</div>';
				}
			?>
			<br>
			<form action="index.php" method="post">
  
				<input type="hidden" name="action" value="register">
				
				
				<div class="form-group">
					<label for="username"><b>Username</b></label>
					<input type="text" class="form-control" placeholder="Enter Username" name="username" required>
				</div>
				<div class="form-group">
					<label for="password"><b>Password</b></label>
					<input type="password" class="form-control" placeholder="Enter Password" name="password" required>
				</div>
				<div class="form-group">
					<label for="gender"><b>Gender</b></label>
					<select class="form-control" name="gender" required>
						<option value=""></option>
						<?php
							$genders = executeSqlStatement($db, "SELECT genderID, genderNAme FROM genders;" )->fetchAll(PDO::FETCH_ASSOC);
							foreach($genders as $gender){
								echo '<option value="'.$gender['genderID'].'">'.$gender['genderName'].'</option>';
							}
						?>
					</select> 			
				</div>					
				<br>
				<button type="submit">Register</button>
				<button type="button" class="cancelbtn" onclick="window.location='index.php';">Cancel</button>

				
			</form> 
			
		</div>
		
	<body>

</html>