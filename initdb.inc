<?php 
	
	session_start();
	
	//Open the application database then create and populate tables if required
	
			
	try {
	
		//open the database
		$db = new PDO('sqlite:site.sq3');
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//check for table 'contents'
		if( tableExists($db, "contents") == FALSE ){
			// table 'contents' does not exist. Create and populate it;		
			executeSqlStatement($db, "CREATE TABLE IF NOT EXISTS 'contents' ('cid'	integer, 'body' text, PRIMARY KEY('cid'));" );
			executeSqlStatement($db, "INSERT INTO 'contents' ('cid','body') VALUES (1,'the answer will be revealed after you have logged in.');" );
			executeSqlStatement($db, "INSERT INTO 'contents' ('cid','body') VALUES (2,'the answer is 42.');" );
		} 
	
		//check for table 'genders'
		if( tableExists($db, "genders") == FALSE ){
			//table 'genders' does not exist so initialize it
			executeSqlStatement($db, "CREATE TABLE IF NOT EXISTS 'genders' ( 'genderID' INTEGER NOT NULL UNIQUE, 'genderName' TEXT NOT NULL, PRIMARY KEY('genderID'));" );
			executeSqlStatement($db, "INSERT INTO 'genders' ('genderID','genderName') VALUES (1,'Male');" );
			executeSqlStatement($db, "INSERT INTO 'genders' ('genderID','genderName') VALUES (2,'Female');" );
			executeSqlStatement($db, "INSERT INTO 'genders' ('genderID','genderName') VALUES (3,'Other');" );
		}
		
		//check for table 'users'
		if( tableExists($db, "users") == FALSE ){
			// table 'users' does not exist. Create and populate it with a default test user;
			executeSqlStatement($db, "CREATE TABLE IF NOT EXISTS 'users' ( 'username' TEXT NOT NULL UNIQUE, 'password' TEXT NOT NULL, 'gender' INTEGER NOT NULL REFERENCES genders(genderID), PRIMARY KEY('username'));");
			$hash = password_hash("password", PASSWORD_DEFAULT);
			$params = array('Testuser', $hash, 1);
			executeSqlStatement($db, "INSERT INTO 'users' ('username','password','gender') VALUES (?, ?, ?);", $params );
		}		

	} catch (PDOException $e) {
        // We got an exception
		header("Location: error.php");
		die();
    }
	
	function tableExists($pdo, $table) {
		// Try a select statement against the table
		try {
			$result = $pdo->query("SELECT 1 FROM $table LIMIT 1");
		} catch (Exception $e) {
			// We got an exception == table not found
			return FALSE;
		}
		// Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
		return $result !== FALSE;
	}
	
	function executeSqlStatement($db, $sql, $params = array()) {
		try {
			$stmt = $db->prepare($sql);
			$stmt->execute($params);
		}
		catch (PDOException $e){
			//send user to error page in case of any unexpected issues
			header("Location: error.php?".$e);
//			die();
		}
		return $stmt;
	}
?>