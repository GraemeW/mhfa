<?php

	// open database and initialize tables if needed
	include 'initdb.inc';	

?>

<!doctype html>

<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<title>Basic PHP Page - Login</title>
		<link rel="stylesheet" href="mhfa.css">
	</head>

	<body>

		<div class="container">
			
			<h1>Login Form</h1>
			<br>
			<?php
				if(isset($_GET['username'])){
					echo'<div class="alert alert-success">Registration successful! Please login using your registered username and password.</div>';
				}
				if(isset($_GET['invalid'])){
					echo'<br><div class="alert alert-danger">Invalid username or password. Try again or <a href="register.php">register</a>.</div>';
				}
				if(isset($_GET['missing'])){
					echo'<br><div class="alert alert-danger">Please enter both a username and a password. Try again or <a href="register.php">register</a>.</div>';
				}
			?>  
			<form action="index.php" method="post">
  
				<input type="hidden" name="action" value="login">
				
				<div class="form-group">
					<label for="username"><b>Username</b></label>
					<input type="text" class="form-control" placeholder="Enter Username"  name="username" required
						<?php if(isset($_GET['username'])){ echo 'value="'.$_GET['username'].'" '; } ?>  
					>
				</div>
				<div class="form-group">
					<label for="password"><b>Password</b></label>
					<input type="password" class="form-control" placeholder="Enter Password" name="password" required
						<?php if(isset($_GET['username'])){ echo ' autofocus '; } ?> 
					>
				</div>
				<br>
				<button type="submit">Login</button>
				<button type="button" class="cancelbtn" onclick="window.location='index.php';">Cancel</button>
				

				
			</form> 
			
		</div>
	
	</body>
	
</html>