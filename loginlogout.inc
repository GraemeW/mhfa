<?php

	// executed by index.php to process a submitted login form
	$action = '';
	
	if(empty($_POST['action'])){
		//no post action, check GET vars
		if(empty($_GET['action'])){
			//no GET action
		} else {
			$action = $_GET['action'];
		}	
	} else {
		$action = $_POST['action'];
	}

	switch ($action) {
		case 'login':
			login($db);
			break;
		case 'logout':
			logout();
			break;
		default:
			//no action or unknown login action - do nothing
			break;
	}


	function logout(){
		//kill current session and return to home page
		session_destroy();
		header("Location:index.php");
		die();
	}

	function login($db){
		//validate login request and - 
		//  on success initialize session and keep loading index.php
		//  on fail return to login form/page with to display an error and let user try again
	    
		//whether user was already logged in or not, kill the session
		session_destroy();
	
		$username = trim($_POST['username']);
		$password = trim($_POST['password']);	

		//need a username and a pw - return to login screen and show error_get_last
		//in this case, bootstrap should ensure un/pw is present but check anyway
		if(empty($_POST['username']) || empty($_POST['password'])){ 
			header("Location: login.php?missing");
			die();
		}
		if($username=='' || $password=='') {
			header("Location: login.php?missing");
			die();
		}
		
		//lookup username in database and get hashed password
		$params = array($username);
		$hashedpw = executeSqlStatement($db, "select password FROM users WHERE username = ?;", $params )->fetchColumn();

		if($hashedpw==''){
			header("Location: login.php?invalid");
			die();
		}
		
		
		if (password_verify($password, $hashedpw)) {
			//Password is valid!
			session_start();
			$_SESSION['username'] = $username;
			//header("Location:index.php?valid-".$_SESSION['username']);
		} else {
			header("Location: login.php?invalid");
			die();
		}

	}

?>