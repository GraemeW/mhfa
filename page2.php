<?php

	// open database and initialize tables if needed
	include 'initdb.inc';
	
?>

<!doctype html>

<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<title>Page 2</title>
		<link rel="stylesheet" href="mhfa.css">
	</head>

	<body>

		<div class="container">
			
			<h1>Page 2</h1>
			
			<?php
				include 'mainmenu.inc'; 
			?>
			
			<br>
			<p>	
				This is Page 2
			</p>
				
		
		</div>
		
	</body>
</html>