<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<title>Error</title>
		<link rel="stylesheet" href="mhfa.css">
	</head>

	<body>
		<h1>An error occured</h1>
		<p>
			Please <a href="index.php">try again</a> or contact the application administrator on 555-...
		</p>
	</body>

</html>