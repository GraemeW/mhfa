<?php

	// open database and initialize tables if needed
	include 'initdb.inc';
	
	// deal with login and logout requests
	include 'loginlogout.inc';
	
	//process registrations
	include 'processregistration.inc';
	

?>

<!doctype html>

<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<title>Home Page</title>
		<link rel="stylesheet" href="mhfa.css">
	</head>

	<body>

		<div class="container">
			
			<h1>Home Page</h1>
			
			<?php
				include 'mainmenu.inc'; 
			?>
			
			
			<p>	
				<?php 
					if(empty($_SESSION['username'])){
						$cid = 1;
					} else {
						$cid = 2;
					}
					
					$body = executeSqlStatement($db, "SELECT body FROM contents WHERE cid = ?;", array($cid) )->fetchColumn();
					if($body==''){
						echo 'Please put some text into table contents column body.';
					} else {
						//echo $body; 
						echo'<div class="alert alert-info">'.$body.'</div>';
				
					}
				?>
			</p>
				
		
		</div>
		
	</body>
</html>