<?php

	// executed by index.php to process a submitted registration form
	$action = '';
	
	if(empty($_POST['action'])){
		//no post action, check get vars
		if(empty($_GET['action'])){
			//no get action
		} else {
			//use the action specified by the GET action parameter
			$action = $_GET['action'];
		}	
	} else {
		$action = $_POST['action'];
	}


	switch ($action) {
		case 'register':
			register($db);
			break;
		default:
			//unknown registration action - do nothing
			break;
	}

	function register($db){
		//validate submitted registration and - 
		//  on success send user to login with new username
		//  on fail return to registration form/page with to display an error and let user try again
	    
		//whether user was already logged in or not, kill the session	    
		session_destroy();
	
		$username = trim($_POST['username']);
		$password = trim($_POST['password']);
		$gender = trim($_POST['gender']);

		//need a username, pw and gender - return to registration screen and show error_get_last
		//in this case, bootstrap should ensure un/pw/gender is present but check anyway
		if(empty($_POST['username']) || empty($_POST['password']) || empty($_POST['gender'])){ 
			header("Location: register.php?missing");
		}
		if($username=='' || $password=='' || $gender=='') {
			header("Location: register.php?missing");
			die();
		}
		
		//can perfom more validation here, eg password length
		//...
		
		//see if this username is already registered
		$usercount = executeSqlStatement($db, "SELECT COUNT(*) FROM users WHERE username = ?;", array($username) )->fetchColumn();
		 
		if($usercount!=='0'){
			//The given username is already in the users table - send back to registration form and inform user
			header("Location: register.php?used");
			die();
		}

		//Create new user and store with hashed password
		$hashedpassword = password_hash($password, PASSWORD_DEFAULT);
		$params = array($username, $hashedpassword, $gender);
		executeSqlStatement($db, "INSERT INTO 'users' ('username','password','gender') VALUES (?, ?, ?);", $params );
		
		//Send user to login screen
		header("Location: login.php?username=".$username);
		die();
	}
	
?>